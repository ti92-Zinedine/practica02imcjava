package com.example.practica02imcjava;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {
    private EditText txtAltura;
    private EditText txtPeso;
    private TextView lblIMC;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Relacionar los objetos
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        lblIMC = (TextView) findViewById(R.id.lblIMC);
        btnCalcular = (Button) findViewById(R.id.btnCalcularIMC);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        // Codificar el evento clic del botón Calcular IMC
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Validar inputs vacíos
                if(txtAltura.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Campo de altura vacío", Toast.LENGTH_SHORT).show();
                } else if(txtPeso.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Campo de peso vacío", Toast.LENGTH_SHORT).show();
                } else{
                    // Obtener los valores decimales de altura y peso
                    double altura = Double.parseDouble(txtAltura.getText().toString());
                    double peso = Double.parseDouble(txtPeso.getText().toString());

                    // Validar los valores ingresados
                    if(altura <= 0 || peso <= 0){
                        Toast.makeText(MainActivity.this, "Ingrese valores válidos", Toast.LENGTH_SHORT).show();
                    }else {
                        // Calcular el IMC y convertir la altura de cm a m
                        altura=altura/100;
                        double imc = peso/(altura * altura);
                        // Establecer un formato de dos decimales
                        DecimalFormat df = new DecimalFormat("#.00");

                        // Mostrar el resultado del IMC del usuario
                        lblIMC.setText("Resultado IMC: " + df.format(imc) + " kg/m2");
                    }
                }
            }
        });

        // Codificar el evento clic del botón Limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Validar inputs vacíos
                if(txtAltura.getText().toString().isEmpty() && txtPeso.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Los campos ya están vacíos", Toast.LENGTH_SHORT).show();
                } else {
                    // Se limpian los campos y etiquetas
                    txtAltura.setText("");
                    txtAltura.setHint("Altura");
                    txtPeso.setText("");
                    txtPeso.setHint("Peso");
                    lblIMC.setText("Resultado IMC:");
                }

            }
        });

        // Codificar el evento clic del botón Cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Se cierra la App
                finish();
            }
        });

    }
}